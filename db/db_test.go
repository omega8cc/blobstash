package db

import (
	"bytes"
	"sync"
	"testing"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func TestDB(t *testing.T) {
	db, err := New("db_base")
	defer db.Destroy()
	if err != nil {
		t.Fatalf("Error creating db %v", err)
	}

	err = db.put([]byte("foo"), []byte("bar"))
	check(err)

	val, err := db.get([]byte("foo"))
	check(err)

	if !bytes.Equal(val, []byte("bar")) {
		t.Errorf("Error getting value")
	}

	valmissing, err := db.get([]byte("foomissing"))
	check(err)

	if valmissing != nil {
		t.Errorf("Non existent key should return nil")
	}

	err = db.put([]byte("foo"), []byte("10"))
	check(err)

	val, err = db.get([]byte("foo"))
	check(err)

	if !bytes.Equal(val, []byte("10")) {
		t.Errorf("Error getting value")
	}

	err = db.del([]byte("foo"))
	check(err)

	val, err = db.get([]byte("foo"))
	check(err)
	if val != nil {
		t.Error("Deleted key should return nil")
	}

	err = db.incrby([]byte("foo"), 50)
	check(err)

	val, err = db.get([]byte("foo"))
	check(err)
	if !bytes.Equal(val, []byte("50")) {
		t.Error("Error incrby")
	}

	// testing low level binary encoded uint32
	valUint, err := db.getUint32([]byte("foo2"))
	check(err)

	if valUint != uint32(0) {
		t.Errorf("Uninitialized uint32 key should return 0")
	}

	err = db.putUint32([]byte("foo2"), uint32(5))
	check(err)

	valUint, err = db.getUint32([]byte("foo2"))
	check(err)
	if valUint != uint32(5) {
		t.Error("Key should be set to 5")
	}

	err = db.incrUint32([]byte("foo2"), 2)
	check(err)

	valUint, err = db.getUint32([]byte("foo2"))
	check(err)
	if valUint != uint32(7) {
		t.Error("Key should be set to 7")
	}

	var wg sync.WaitGroup
	// Test the mutex
	for i := 0; i < 50; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			err = db.incrUint32([]byte("foo3"), 1)
			check(err)
		}()
	}
	wg.Wait()

	valUint, err = db.getUint32([]byte("foo3"))
	check(err)
	if valUint != uint32(50) {
		t.Errorf("Key foo3 should be set to 10, got %v", valUint)
	}

	kvs, err := GetRange(db.db, []byte("foo2"), []byte("foo3"), 0)
	check(err)
	if len(kvs) != 2 {
		t.Errorf("Range len should be %v, got %v", 2, len(kvs))
	}

	// TODO(tsileo) check range with KVIs

	db.Close()
	err = db.Destroy()
	if err != nil {
		panic(err)
	}
}
